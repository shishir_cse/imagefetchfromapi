//
//  InfinitieViewController.swift
//  FetchDataFromAPI
//
//  Created by Saddam on 22/11/21.
//

import UIKit
import Kingfisher

class InfinitieViewController: UIViewController {

    @IBOutlet weak var numOfImgLbl: UILabel!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var infinitieCollectonView: CarouselCollectionView!
    
    var  imageInfoArray = [allImagesUrl]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infinitieCollectonView.carouselDataSource = self
        //infinitieCollectonView.carouse = self
        
        infinitieCollectonView.register(UINib(nibName: "ScrollCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ScrollCollectionViewCell")
        
        infinitieCollectonView.isAutoscrollEnabled = true
        infinitieCollectonView.autoscrollTimeInterval = 1.2
        
        let size = UIScreen.main.bounds.size
        infinitieCollectonView.flowLayout.itemSize = CGSize(width: size.width, height: size.height)
       // infinitieCollectonView.currentPage = 4
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let size = UIScreen.main.bounds.size
        infinitieCollectonView.flowLayout.itemSize = CGSize(width: size.width, height: size.height-navBarView.frame.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        infinitieCollectonView.isAutoscrollEnabled = true
    }
    
    
    @IBAction func backBtnAction(_ sender: Any){
        infinitieCollectonView.isAutoscrollEnabled = false
        navigationController?.popViewController(animated: true)
        
    }
    
}
extension InfinitieViewController: CarouselCollectionViewDataSource{
    var numberOfItems: Int {
        return imageInfoArray.count
    }
    
    func carouselCollectionView(_ carouselCollectionView: CarouselCollectionView, cellForItemAt index: Int, fakeIndexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = infinitieCollectonView.dequeueReusableCell(withReuseIdentifier: "ScrollCollectionViewCell", for: fakeIndexPath) as! ScrollCollectionViewCell
        
        cell.numOfLbl.text = "\(index)"
        loadImage(url: URL(string: imageInfoArray[index].download_url!)!, cell: cell)
        return cell
    }
    
    func carouselCollectionView(_ carouselCollectionView: CarouselCollectionView, didSelectItemAt index: Int) {
      print("Did select item at \(index)")
        
        
          guard let zoomVc = UIStoryboard(name: "Zoom", bundle: nil).instantiateViewController(withIdentifier: "ZoomViewController") as? ZoomViewController else {return}
        
        if let urlStr = imageInfoArray[index].download_url{
            
            zoomVc.imageUrl =   URL(string: urlStr)
        }
        infinitieCollectonView.isAutoscrollEnabled = false
        navigationController?.pushViewController(zoomVc, animated: true)
        
        
    }

    func carouselCollectionView(_ carouselCollectionView: CarouselCollectionView, didDisplayItemAt index: Int) {
     // pageControl.currentPage = index
    }
    
    
    func loadImage(url: URL, cell: ScrollCollectionViewCell){
        
        KingfisherManager.shared.retrieveImage(with: url) { result in
            let image = try? result.get().image
            if let image = image {
                cell.imageViewOutlet.image = image
               
                self.addZoombehavior(for: cell.imageViewOutlet)
               // ProgressHUD.dismiss()
            }
        }
    }
    
}


